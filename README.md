# OpenBayes CLI Releases

Please see [Releases][url] tab for downloads, you can check [our docs][doc] for how to install it for your system.

请访问 [Releases][url] 标签查看下载；查看 [我们的文档][doc] 了解如何安装命令行工具。

[url]: https://gitee.com/openbayes/bayes-releases
[doc]: https://openbayes.com/docs/cli/


# OpenBayes CLI Homebrew Tap Formula

You can install this tap formula with:

你也可以使用此命令安装：

```bash
brew tap openbayes/bayes https://gitee.com/openbayes/bayes-releases 
brew install bayes
```

[View more about our CLI tools](https://openbayes.com/docs/cli/)

[更多资讯请查看我们的 CLI 文档](https://openbayes.com/docs/cli/)